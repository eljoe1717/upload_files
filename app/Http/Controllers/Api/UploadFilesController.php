<?php

namespace App\Http\Controllers\Api;

use App\Gallery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UploadFilesController extends Controller
{
    public $audioMime = ['au','kar','m3u','m4a','m4b','m4p','mid','midi','mp2','mp3','mpga','ra','ram','snd','wav'];
    public $videoMime = ['avi','dif','dv','m4u','m4v','mov','movie','mp4','mpe','mpeg','mpg','mxu','qt'];
    public $imageMime = ['bmp','cgm','djv','djvu','gif','ico','ief','jp2','jpe','jpeg','jpg','mac','pbm','pct','pgm','pic','pict','png','pnm','pnt','pntg','ppm','qti','qtif','ras','rgb','svg','tif','tiff','wbmp','xbm','xpm'];
    public function uploadFiles(Request $request)
    {
        ini_set('post_max_size',-1);
        ini_set('upload_max_filesize',-1);
        $mimes = array_merge($this->audioMime,$this->videoMime,$this->imageMime);
        $mimes = implode(',',$mimes);
        $validation = Validator::make($request->all(),[
            '_files'=>'required|array',
            '_files.*'=> 'required|mimes:'.$mimes
        ],[
            '_files.*.mimes'=>'the File must be a video , image or audio'
        ]);
        if ($validation->fails()) {
            return response()->json(['msg'=>$validation->errors()->first()],404);
        }

        if (count($request->files) > 0) {
            $gallery = Gallery::create();
        }

        foreach ($request->_files as $value) {
            $file = $this->uploader('gallery_files',$value);
            $gallery->files()->create([
                'file'=>$file
            ]);
        }
        return response()->json(['msg'=>"uploaded successfully!"]);
    }

    public function uploader($path,$file)
    {
        return Storage::disk('public')->putFile($path, $file);
    }
}
