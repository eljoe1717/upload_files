<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $guarded = [];

    public function files()
    {
        return $this->hasMany(GalleryFile::class,'gallery_id');
    }
}
