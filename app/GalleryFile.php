<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryFile extends Model
{
    protected $guarded = [];

    public function getFileAttribute()
    {
        $filename = $this->attributes['file'];
        if (!empty($filename)) {
            $base_url = url('/');
            return $base_url . '/storage/' . $filename;
        } else {
            return null;
        }
    }
}
